{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

main :: IO ()
main = do
    x <- getArgs
    file <- readFile (head x)
    let res = mapM_ putStrLn (lines file)
    print res
