"use strict";

// base de données
var pgp = require('pg-promise')();
var cn = 'postgres://bmxrideruser:bmxrideruser@localhost:5432/bmxriders';
var db = pgp(cn);

// serveur web
const express = require("express");
const app = express();

app.use("/", express.static(__dirname + "/static"));

app.get("/bmxriders", function (req, res) {
    db.query('SELECT name, image FROM bmxrider')
        .then(users => users.map(x => `${x.name};${x.image}`))
        .then(users => users.join("\n"))
        .then(r => res.send(r))
        .catch(error => res.send(error.toString()));
});

const port = 5000;
const server = app.listen(port, function () {
  console.log(`Listening on port ${port}...`);
});

