{-# LANGUAGE OverloadedStrings #-}
import Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Lucid
import qualified Clay as C
import           Web.Scotty (scotty, get, post, html, param, rescue)

mainPage :: Html()
mainPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "The helloscotty project !"
            a_ [href_ "/hello"] "go to hello page"


helloPage :: String -> Html()
helloPage name = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ $ toHtml $ "Hello " ++  name
            form_ [action_ "/hello", method_ "get"] $ do
                input_ [name_ "name", value_ "enter text here"]
                input_ [type_ "submit"]
            a_ [href_ "/"] "go to home page"


main = scotty 3000 $ do

    get "/" $ do
        html . renderText $ mainPage
        -- http://localhost:3000/

    get "/hello" $ do
        name <- param "name" `rescue` (\_ -> return "")
        html . renderText $ helloPage name
        -- http://localhost:3000/name1/toto




