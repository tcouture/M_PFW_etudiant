{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

-- TODO implement CSS styles
-- TODO implement route views


myCss :: C.Css
myCss = do
    C.a C.# C.byClass "aCss" C.? do
        C.textDecoration  C.none
        C.color           C.inherit
    C.body C.? do
        C.backgroundColor  C.azure
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black
        C.margin           (C.em 1) (C.em 1) (C.em 1) (C.em 1)
        C.width            (C.px 320)
        C.float            C.floatLeft
    C.img C.# C.byClass "imgCss" C.? do
        C.width            (C.px 320)
        C.height           (C.px 240)
    C.p C.# C.byClass "pCss" C.? do
        C.fontWeight C.bold


mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ do
    style_ $ L.toStrict $ C.render $ myCss
    title_ titleStr
  body_ page

poemFormat :: Model.Poem -> Html()
poemFormat poem = do
  div_ [class_ "divCss"] $ do
    p_ [class_ "pCss"] $ toHtml $ Model.title poem
    p_ $ toHtml $ "by " ++  T.unpack (Model.author poem)

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do
  h1_ "Poem hub"
  div_ $ do
    toHtml $ mapM_ poemFormat poems

