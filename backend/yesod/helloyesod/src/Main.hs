{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

import CodeCaesarText
import MusicPersist

import           Control.Applicative ((<$>), (<*>))
import           Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T
import           Data.Maybe (maybe)
import           Yesod

data App = App

instance Yesod App

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

-- TODO add routes (caesar + music)
mkYesod "App" [parseRoutes|
  /         HomeR     GET
  /caesar   CaesarR   GET
  |]

-- TODO CSS styles

-- TODO link to routes (caesar + music)
getHomeR = defaultLayout $ do
  [whamlet|
    <h1> Home
    <p> <a href=@{CaesarR}> Caesar
    |]

-- TODO caesar (form + html)

getCaesarR = defaultLayout $ do
  [whamlet|
    <h1> Caesar
    <p> TODO
    |]

-- TODO music (form + html)

main = warp 3000 App

