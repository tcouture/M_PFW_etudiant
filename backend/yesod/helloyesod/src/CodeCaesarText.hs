{-# LANGUAGE OverloadedStrings #-}

module CodeCaesarText (codeCaesar) where

import qualified Data.Text as T
import qualified Data.Char as C

data Caesar = Caesar Int T.Text deriving (Show)

ordA :: Int
ordA = C.ord 'a'

shiftChar :: Int -> C.Char -> C.Char
shiftChar n c | C.isLower c = C.chr $ ordA + ((n - ordA + (C.ord c)) `mod` 26)
              | otherwise = c

codeCaesar :: Caesar -> T.Text
codeCaesar (Caesar n t) = T.map (shiftChar n) t

