{-# LANGUAGE OverloadedStrings #-}

-- import qualified Clay as C
-- import qualified Data.Text as T
-- import qualified Data.Text.IO as TIO
-- import qualified Data.Text.Lazy as L
-- import           Lucid
import           System.Environment (getArgs, getProgName)
import Lucid
import           Data.Text.Lazy (toStrict)
import qualified Clay as C

myCss :: C.Css
myCss = C.div C.? do
  C.backgroundColor  C.beige
  C.border           C.solid (C.px 1) C.black

main :: IO ()
main = do
    args <- getArgs
    if length args /= 2
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <input dat> <output html>"
    else do
        file <- readFile (head args)
        let res = lines file
        print $ renderText (myPage res)
        renderToFile (last args) (myPage res)

listtoLi :: [String] -> Html()        
listtoLi x = mapM_ (li_ . toHtml ) x


myPage :: [String] -> Html()
myPage lines = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ toStrict $ C.render myCss
        body_ $ do
            h1_ "My List"
            div_ $ do
                ul_ $ do
                    listtoLi lines
              


