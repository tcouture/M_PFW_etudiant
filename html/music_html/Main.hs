{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.List
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import qualified Database.PostgreSQL.Simple as SQL
import Lucid
import           Data.Text.Lazy (toStrict)
import qualified Clay as C

data Music = Music
    { _title :: T.Text,_artist :: T.Text
    } deriving (Show)

instance FromRow Music where
    fromRow = Music <$> field <*> field

myCss :: C.Css
myCss = C.div C.? do
    C.backgroundColor  C.beige
    C.border           C.solid (C.px 1) C.black

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='127.0.0.1' port=5432 dbname=mybd user=toto password='toto'"

    -- TODO
    res <- SQL.query_ conn "SELECT titles.name as title , (SELECT name FROM artists WHERE titles.artist = artists.id ) as artist FROM titles" :: IO [(Music)]
    mapM print res

    SQL.close conn

    print $ renderText (myPage res)
    renderToFile "index.html" (myPage res)

listtoLi :: [Music] -> Html()        
listtoLi x = mapM_ (li_ . toHtml . musictoString ) x

musictoString :: Music -> T.Text
musictoString (Music a b ) =   T.concat [a, " - ", b]

myPage :: [Music] -> Html()
myPage lines = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ toStrict $ C.render myCss
        body_ $ do
            h1_ "My Music"
            div_ $ do
                ul_ $ do
                    listtoLi lines

