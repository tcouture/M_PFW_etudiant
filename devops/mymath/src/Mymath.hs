-- |
-- Module: Mymath
-- Copyright: (c) 2018 Julien Dehos
-- License: MIT
--
-- Some math functions.
--
-- > import Mymath
-- > square 3 :: Int
module Mymath where

-- |
-- Compute the square of x, i.e. x*x.
square :: Num a => a -> a
square x = x*x

